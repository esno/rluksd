#ifndef LUKSD_CRYPTSETUP_H
#define LUKSD_CRYPTSETUP_H

#include "luksd.h"

int luksd_cryptsetup_read_device(luksd_device_t *device);

#endif
